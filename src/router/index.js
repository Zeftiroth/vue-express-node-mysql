import Vue from "vue";
import Router from "vue-router";
import LoginRegister from "@/pages/LoginRegister";
import LandingPage from "@/pages/LandingPage";
import AllUsers from "@/pages/AllUsers";
import EditProfile from "@/pages/EditProfile";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      name: "LandingPage",
      component: LandingPage
    },
    {
      path: "/allusers",
      name: "AllUsers",
      component: AllUsers
    },
    {
      path: "/loginRegister",
      name: "LoginRegister",
      component: LoginRegister
    },
    {
      path: "/editProfile",
      name: "EditProfile",
      component: EditProfile
    }
  ]
});
